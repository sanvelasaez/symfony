<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/error")
 */
class ErrorController extends AbstractController
{
    /**
     * @Route("/error403", name="error403")
     */
    public function error403()
    {
        return $this->render('error/deny_user.html.twig', [
            'controller_name' => 'ErrorController',
        ]);
    }
}
