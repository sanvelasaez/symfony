<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200207080859 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__ciudades AS SELECT id, nombre_ciu FROM ciudades');
        $this->addSql('DROP TABLE ciudades');
        $this->addSql('CREATE TABLE ciudades (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre_ciu VARCHAR(40) NOT NULL)');
        $this->addSql('INSERT INTO ciudades (id, nombre_ciu) SELECT id, nombre_ciu FROM __temp__ciudades');
        $this->addSql('DROP TABLE __temp__ciudades');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_FF77006A22E9141 ON ciudades (nombre_ciu)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX UNIQ_FF77006A22E9141');
        $this->addSql('CREATE TEMPORARY TABLE __temp__ciudades AS SELECT id, nombre_ciu FROM ciudades');
        $this->addSql('DROP TABLE ciudades');
        $this->addSql('CREATE TABLE ciudades (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, nombre_ciu VARCHAR(40) DEFAULT NULL COLLATE BINARY)');
        $this->addSql('INSERT INTO ciudades (id, nombre_ciu) SELECT id, nombre_ciu FROM __temp__ciudades');
        $this->addSql('DROP TABLE __temp__ciudades');
    }
}
