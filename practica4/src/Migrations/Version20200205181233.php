<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200205181233 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, roles, nombre, apellidos, fecha_nacimiento, sexo, foto, ciudad_id, email, password FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, roles CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , nombre VARCHAR(30) NOT NULL COLLATE BINARY, apellidos VARCHAR(50) NOT NULL COLLATE BINARY, fecha_nacimiento DATE NOT NULL, sexo VARCHAR(10) NOT NULL COLLATE BINARY, foto VARCHAR(255) NOT NULL COLLATE BINARY, email VARCHAR(40) NOT NULL COLLATE BINARY, password VARCHAR(40) NOT NULL COLLATE BINARY, ciudad_id INTEGER DEFAULT NULL)');
        $this->addSql('INSERT INTO user (id, roles, nombre, apellidos, fecha_nacimiento, sexo, foto, ciudad_id, email, password) SELECT id, roles, nombre, apellidos, fecha_nacimiento, sexo, foto, ciudad_id, email, password FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__user AS SELECT id, email, roles, password, nombre, apellidos, fecha_nacimiento, sexo, foto, ciudad_id FROM user');
        $this->addSql('DROP TABLE user');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, email VARCHAR(40) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(40) NOT NULL, nombre VARCHAR(30) NOT NULL, apellidos VARCHAR(50) NOT NULL, fecha_nacimiento DATE NOT NULL, sexo VARCHAR(10) NOT NULL, foto VARCHAR(255) NOT NULL, ciudad_id INTEGER NOT NULL)');
        $this->addSql('INSERT INTO user (id, email, roles, password, nombre, apellidos, fecha_nacimiento, sexo, foto, ciudad_id) SELECT id, email, roles, password, nombre, apellidos, fecha_nacimiento, sexo, foto, ciudad_id FROM __temp__user');
        $this->addSql('DROP TABLE __temp__user');
    }
}
