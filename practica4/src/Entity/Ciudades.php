<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CiudadesRepository")
 */
class Ciudades
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="ciudad")
     */
    private $nombre_ciudad;

    /**
     * @ORM\Column(type="string", length=40, unique=true)
     */
    private $nombreCiu;

    public function __construct()
    {
        $this->nombre_ciudad = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombreCiu;
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|User[]
     */
    public function getNombreCiudad(): Collection
    {
        return $this->nombre_ciudad;
    }

    public function getNombreCiu(): ?string
    {
        return $this->nombreCiu;
    }

    public function setNombreCiu(?string $nombreCiu): self
    {
        $this->nombreCiu = $nombreCiu;

        return $this;
    }
    
    public function addNombreCiudad(User $nombreCiudad): self
    {
        if (!$this->nombre_ciudad->contains($nombreCiudad)) {
            $this->nombre_ciudad[] = $nombreCiudad;
            $nombreCiudad->setCiudad($this);
        }

        return $this;
    }

    public function removeNombreCiudad(User $nombreCiudad): self
    {
        if ($this->nombre_ciudad->contains($nombreCiudad)) {
            $this->nombre_ciudad->removeElement($nombreCiudad);
            // set the owning side to null (unless already changed)
            if ($nombreCiudad->getCiudad() === $this) {
                $nombreCiudad->setCiudad(null);
            }
        }

        return $this;
    }

    
}
